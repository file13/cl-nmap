#|

cl-nmap

TODO:

document the sax class
async future nmap
add proper package and exports

|#

#|
(eval-when (:compile-toplevel :load-toplevel :execute)
  (ql:quickload '(alexandria cxml uiop
                  cl-ppcre split-sequence grimoire)))
|#

(in-package :cl-user)
(defpackage cl-nmap
  (:use :cl))
(in-package :cl-nmap)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; helper functions to disect nmap XML.


;; I'm keeping this for future debugging/tweaking of SAX parser.

(defclass xml-shape (sax:default-handler)
  ((indentation :initform 0
                :accessor indentation)))
  
(defmethod sax:start-element ((handler xml-shape)
                              namespace-uri local-name
                              qname attributes)
  (declare (ignore namespace-uri qname attributes))
  (incf (indentation handler) 2)
  (format t "~VT~A~%" (indentation handler) local-name))

(defmethod sax:end-element ((handler xml-shape)
                            namespace-uri local-name qname)
  (declare (ignore namespace-uri qname))
  (decf (indentation handler) 2))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass xmlshape (sax:default-handler)
  ((indentation :initform 0
                :accessor indentation)
   (buffer :initform nil
           :accessor buffer)))

(defmethod sax:characters ((handler xmlshape) data)
  (setf (buffer handler) data))

(defmethod sax:start-element ((handler xmlshape)
                              namespace-uri local-name
                              qname attributes)
  (declare (ignore namespace-uri qname))
  (incf (indentation handler) 2)
  (format t "~VTLocalname: ~A=~{Attributes: |~A|~^, ~}~%"
          (indentation handler)
          local-name
          (mapcar #'sax:attribute-value attributes)))

(defmethod sax:end-element ((handler xmlshape)
                            namespace-uri local-name qname)
  (declare (ignore namespace-uri qname))
  (incf (indentation handler) 2)
  (format t "~VTCharacters:|~A|~%" (indentation handler) (buffer handler))
  (decf (indentation handler) 4))


;; taken from http://cl-cookbook.sourceforge.net/strings.html
(defun replace-all (string part replacement &key (test #'char=))
"Returns a new string in which all the occurences of the part 
is replaced with replacement."
    (with-output-to-string (out)
      (loop with part-length = (length part)
            for old-pos = 0 then (+ pos part-length)
            for pos = (search part string
                              :start2 old-pos
                              :test test)
            do (write-string string out
                             :start old-pos
                             :end (or pos (length string)))
            when pos do (write-string replacement out)
            while pos)))

(defun as-keyword (s)
  "Makes a keyword in 'proper' uppercase with no spaces."
  (alexandria:make-keyword (replace-all (string-upcase s) " " "-")))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

#|

This is the SAX parser for Nmap XML output.

I have not gone through the DTD to make sure we have everything at this time.

We store everything in a hash table of alists.

Since there are multiple hosts, we store the hosts in a vector of alists.
We do the same for certain sub fields in each host like scripts.

|#

(defclass nmap-results (sax:default-handler)
  ((data :initform (make-hash-table :test #'equal)
         :accessor data)
   (host :initform '()
         :accessor host)
   (all-hosts :initform (make-array 1 :adjustable t :fill-pointer 0)
              :accessor all-hosts)
   (addressess :initform (make-array 1 :adjustable t :fill-pointer 0)
               :accessor addressess)
   (address :initform '()
            :accessor address)
   (hostnames :initform (make-array 1 :adjustable t :fill-pointer 0)
              :accessor hostnames)
   (hostname :initform '()
             :accessor hostname)
   (ports :initform (make-array 1 :adjustable t :fill-pointer 0)
          :accessor ports)
   (port :initform '()
         :accessor port)
   ;; since we can have a script tage in either ports or hostscript
   (in-portsp :initform nil
              :accessor in-portsp)
   (hostscript :initform (make-array 1 :adjustable t :fill-pointer 0)
               :accessor hostscript)
   (script :initform '()
           :accessor script)
   (all-os :initform (make-array 1 :adjustable t :fill-pointer 0)
               :accessor all-os)
   (os :initform '()
       :accessor os)
   (traceroute :initform (make-array 1 :adjustable t :fill-pointer 0)
               :accessor traceroute)
   (hop :initform '()
        :accessor hop)
   (last-attributes :initform nil
                    :accessor last-attributes)
   (buffer :initform nil
           :accessor buffer)
   ))

(defmethod add-hash ((handler nmap-results)
                     local-name attributes)
  "Adds the local-name as a keyword to data with the attributes
as a keyword alist."
  (setf (gethash (as-keyword local-name) (data handler))
        (alexandria:plist-alist
         (alexandria:flatten
          (loop for i in attributes
             collect (list (as-keyword (sax:attribute-qname i))
                           (sax:attribute-value i)))))))

(defmethod add-host ((handler nmap-results)
                     local-name attributes)
  "Adds the local-name as a keyword to data with the attributes
as a keyword alist with a local-name prepended to the name."
  (let ((r (alexandria:flatten
            (loop for i in attributes
               collect
                 (list (as-keyword (format nil "~A-~A" local-name
                                           (sax:attribute-qname i)))
                       (sax:attribute-value i))))))
    (when r
      (push r (host handler)))))

(defmethod add-to-all-hosts ((handler nmap-results))
  "Add the hosts to the all-hosts vector."
  (let ((r (host handler)))
    (when r
      (vector-push-extend
       (alexandria:plist-alist
        (alexandria:flatten (reverse (host handler)))) (all-hosts handler))
      (setf (host handler) '()))))

(defmethod add-to-list-slot ((handler nmap-results)
                             slot local-name attributes)
  "Generic updater to add a new item to the vector accessor."
  (let ((r (alexandria:flatten
            (loop for i in attributes
               collect
                 (list (as-keyword (sax:attribute-qname i))
                       (sax:attribute-value i))))))
    (when r
      (push r (slot-value handler slot)))))

(defmethod add-elem-to-list-slot ((handler nmap-results)
                                  slot local-name)
  "Generic updater to add a new elem item to the vector accessor. This must be called in
sax:end-element or the buffer will not contain the correct info."
  (let ((r (alexandria:flatten
            (loop for i in (last-attributes handler)
               collect
                 (cond ((string= (sax:attribute-qname i) "key")
                        (list (as-keyword (sax:attribute-value i))
                              (buffer handler)))
                       (t
                        (list (as-keyword (sax:attribute-qname i))
                              (sax:attribute-value i))))))))
    (when r
      (push r (slot-value handler slot)))))


(defmethod add-list-slot-to-vector-slot ((handler nmap-results) list-slot vector-slot)
  "Generic updater to add a vector-slot to a target slot."
  (vector-push-extend
   (alexandria:plist-alist
    (alexandria:flatten (reverse (slot-value handler list-slot))))
   (slot-value handler vector-slot))
  (setf (slot-value handler list-slot) '())) ; reset list-slot

(defmethod finalize-vector-slot ((handler nmap-results) key vector-slot parent-list-slot)
  "Finalizes a vector-slot by pushing it to the key in the parent-list-slot.
Resets the vector-slot afterwards."
  ;; push the vector under the key to the parent slot.
  (push (cons key (slot-value handler vector-slot)) (slot-value handler parent-list-slot))
  ;; reset the vector
  (setf (slot-value handler vector-slot) (make-array 1 :adjustable t :fill-pointer 0)))

(defmethod sax:characters ((handler nmap-results) data)
  "Set the latest character data into the buffer."
  (setf (buffer handler) data))

(defmethod sax:start-element ((handler nmap-results)
                              namespace-uri local-name
                              qname attributes)
  "This runs when we make-instance at the start of each element."
  (declare (ignore namespace-uri qname))
  (setf (last-attributes handler) attributes)
  (cond
    ;; we start by just add-hash'ing the main attributes
    ((string= local-name "nmaprun")
     (add-hash handler local-name attributes))
    ((string= local-name "scaninfo")
     (add-hash handler local-name attributes))
    ((string= local-name "verbose")
     (add-hash handler local-name attributes))
    ((string= local-name "debugging")
     (add-hash handler local-name attributes))

    ;; *** host begins here
    ;; we keep hosts in a separate list until we add them to the vector all-hosts
    ((string= local-name "host")
     (add-host handler local-name attributes))
    ((string= local-name "status")
     (add-host handler local-name attributes))
    ((string= local-name "address")
     (add-to-list-slot handler 'address local-name attributes)
     (add-list-slot-to-vector-slot handler 'address 'addressess))
    ((string= local-name "hostnames")
     ;; add the addressess since this marks the end of ad hoc addressess
     (finalize-vector-slot handler :address 'addressess 'host))
    ((string= local-name "hostname")
     (add-to-list-slot handler 'hostname local-name attributes)
     (add-list-slot-to-vector-slot handler 'hostname 'hostnames))

    ;; we add ports separately.
    ((string= local-name "ports")
     (setf (in-portsp handler) t)
     (add-to-list-slot handler 'port local-name attributes))
    ((string= local-name "extraports")
     (add-to-list-slot handler 'port local-name attributes))
    ((string= local-name "extrareasons")
     (add-to-list-slot handler 'port local-name attributes))
    ((string= local-name "port")
     (add-to-list-slot handler 'port local-name attributes))
    ((string= local-name "state")
     (add-to-list-slot handler 'port local-name attributes))
    ((string= local-name "service")
     (add-to-list-slot handler 'port local-name attributes))
    ;; script, elem, and tables can be in either ports or hostscripts
    ((and (string= local-name "script")
          (in-portsp handler))
     (add-to-list-slot handler 'port local-name attributes))
    ((string= local-name "cpe")
     (add-to-list-slot handler 'port local-name attributes))
    ;; ports finished here

    ;; begin script
    ((string= local-name "hostscript")
     (add-to-list-slot handler 'script local-name attributes))
    ((and (string= local-name "script")
          (not (in-portsp handler)))
     (add-to-list-slot handler 'script local-name attributes))

    ;; os values
    ((string= local-name "os")
     (add-to-list-slot handler 'os local-name attributes))
    ((string= local-name "portused")
     (add-to-list-slot handler 'os local-name attributes))
    ((string= local-name "osfingerprint")
     (add-to-list-slot handler 'os local-name attributes))
    ((string= local-name "osmatch")
     (add-to-list-slot handler 'os local-name attributes)
     (add-list-slot-to-vector-slot handler 'os 'all-os))
    ((string= local-name "osclass")
     (add-to-list-slot handler 'os local-name attributes))

    ((string= local-name "uptime")
     (add-host handler local-name attributes))
    ((string= local-name "distance")
     (add-host handler local-name attributes))
    ((string= local-name "tcpsequence")
     (add-host handler local-name attributes))
    ((string= local-name "ipidsequence")
     (add-host handler local-name attributes))
    ((string= local-name "tcptssequence")
     (add-host handler local-name attributes))
    
    ((string= local-name "hop")
     (add-to-list-slot handler 'hop local-name attributes)
     (add-list-slot-to-vector-slot handler 'hop 'traceroute))
    ((string= local-name "times")
     (add-host handler local-name attributes))
    ;; *** end host

    ((string= local-name "runstats")
     (add-to-all-hosts handler) ; add last host
     (add-hash handler local-name attributes))
    ((string= local-name "finished")
     (add-hash handler local-name attributes))
    ((string= local-name "hosts")
     (add-hash handler local-name attributes))
    ((string= local-name "finished")
     (add-hash handler local-name attributes))
    ;;((string= local-name "hosts")
    ;; (add-hash handler local-name attributes))
    ))

(defmethod sax:end-element ((handler nmap-results)
                            namespace-uri local-name qname)
  "This runs when we make-instance at the end of each element."
  (declare (ignore namespace-uri qname))
  (cond
    ((string= local-name "hostnames")
     (finalize-vector-slot handler :hostnames 'hostnames 'host))
    ((and (string= local-name "elem")
          (in-portsp handler))
     (add-elem-to-list-slot handler 'port local-name))
    ((and (string= local-name "elem")
          (not (in-portsp handler)))
     (add-elem-to-list-slot handler 'script local-name))
    
    ((string= local-name "port")
     (add-list-slot-to-vector-slot handler 'port 'ports))
    ((string= local-name "ports")
     (setf (in-portsp handler) nil)
     (finalize-vector-slot handler :ports 'ports 'host))

    ((and (string= local-name "script")
          (not (in-portsp handler)))
     (add-list-slot-to-vector-slot handler 'script 'hostscript))
    ((string= local-name "hostscript")
     (finalize-vector-slot handler :scripts 'hostscript 'host))
    ((string= local-name "portused")
     (add-list-slot-to-vector-slot handler 'os 'all-os))
    ((string= local-name "osclass")
     (add-list-slot-to-vector-slot handler 'os 'all-os))
    ((string= local-name "osfingerprint")
     (add-list-slot-to-vector-slot handler 'os 'all-os))
    ((string= local-name "os")
     (finalize-vector-slot handler :os 'all-os 'host))
    ((string= local-name "trace")
     (finalize-vector-slot handler :trace 'traceroute 'host))
    
    ;; at the end
    ((string= local-name "host")
     (add-to-all-hosts handler))))

(defmethod sax:end-document ((handler nmap-results))
  "This runs when we make-instance at the start of of the document."
  (setf (gethash :host (data handler))
        (all-hosts handler))
  (data handler))

;;; Parsing interface

(defun parse-nmap-file (filename)
  "Parses an nmap XML result file and returns a nmap-hash."
  (cxml:parse-file filename (make-instance 'nmap-results)))

(defun parse-nmap (xml-string)
  "Parses an nmap XML result string and returns a nmap-hash."
  (cxml:parse xml-string (make-instance 'nmap-results)))

;;; Helper functions

(defun hash-keys (h)
  "Returns all the hash lookup keys."
  (alexandria:hash-table-keys h))

(defun hash-val (h table-key &optional (val-key nil))
  "Return the value of the nmap table key using the value key."
  (if val-key
      (assoc val-key (gethash table-key h))
      (gethash table-key h)))

;;; Nmap Hash accessor functions

(defun nmap-nmaprun (h)
  "Returns the alist of the scaninfo results."
  (gethash :nmaprun h))

(defun nmap-scaninfo (h)
  "Returns the alist of the scaninfo results."
  (gethash :scaninfo h))

(defun nmap-verbose (h)
  "Returns the alist of the verbose results."
  (gethash :verbose h))

(defun nmap-debugging (h)
  "Returns the alist of the debugging results."
  (gethash :debugging h))

(defun nmap-runstats (h)
  "Returns the alist of the runstats results."
  (gethash :runstats h))

(defun nmap-finished (h)
  "Returns the alist of the finished results."
  (gethash :finished h))

(defun nmap-allhosts (h)
  "Returns the alist of the hosts results."
  (gethash :hosts h))

(defun nmap-hosts (h)
  "Returns all hosts as a vector of alists."
  (gethash :host h))

(defun nmap-host (h n &optional (key nil))
  "Returns the host alist of the key for host number n."
  (if key
      (assoc key (aref (gethash :host h) n))
      (aref (gethash :host h) n)))

(defun nmap-ports (h n)
  "Returns vector of port alists for the host number n."
  (cdr (nmap-host h n :ports)))

(defun nmap-scripts (h n)
  "Returns vector of scripts alists for the host number n."
  (cdr (nmap-host h n :scripts)))

(defun nmap-address (h n)
  "Returns vector of address alists for the host number n."
  (cdr (nmap-host h n :address)))

(defun nmap-hostnames (h n)
  "Returns vector of hostnames alists for the host number n."
  (cdr (nmap-host h n :hostnames)))

(defun get-values-in (host vec-key key)
  "Returns a list of all the values in of a vec-key that match the key."
  (ignore-errors
    (remove nil
            (loop for i across (cdr (assoc vec-key host))
               collect
                 (let ((r (assoc key i)))
                   (when r
                     (cdr r)))))))

(defun get-value-in (host vec-key key)
  "Returns the first value from all the values in of a vec-key that match the key."
  (car (get-values-in host vec-key key)))

(defun print-nmap (hash &optional (divider "=>"))
  "Prints a hash."
  (maphash #'
   (lambda (a b)
     (format t "~A ~A ~A~%" a divider b))
   hash))

;;; these below may be dumped.

(defun map-host-vals (h key)
  "Maps all the values with the key from the hosts."
  (map 'vector #'(lambda (i) (cdr (assoc key i))) (gethash :host h)))

(defun map-hosts (h key val)
  "Returns a list of only hosts where key = val."
  (remove nil
          (map 'vector #'(lambda (i)
                           (let ((v (cdr (assoc key i))))
                             (if (string= v val)
                                 i)))
               (gethash :host h))))

(defun map-ports-vals (h n val)
  "Maps a key val across the port vector at host #n."
  (map 'vector #'(lambda (i)
                   (cdr (assoc val i))) (nmap-ports h n)))

;;; Nmap scans

(defun run-nmap (arg-string &key (xml-only nil))
  "Runs an nmap scan and returns the values of the parsed hash followed by the XML output.
If xml-only is t, returns just the XML output."
  (let* ((command (format nil "nmap -oX - ~A" arg-string))
         (xml (uiop:run-program command :output '(:string :stripped t))))
    (if xml-only
        xml
        (values (parse-nmap xml)))))

(defun scan (arg-string targets &key (xml-only nil) (out-file nil))
  "Runs an nmap scan and returns the values of the parsed hash followed by the XML output.
If xml-only is t, returns just the XML output. If out-file specified, the XML is saved as
the filename in addition."
  (let ((args (format nil "~A ~A" arg-string targets)))
    (cond (out-file
           (multiple-value-bind (r xml) (run-nmap args)
             (with-open-file (str out-file
                                  :direction :output
                                  :if-exists :supersede
                                  :if-does-not-exist :create)
               (format str "~A~%" xml))
             r))
          (t
           (run-nmap args :xml-only xml-only)))))

#|

;; to parse an existing scan
(defparameter h (parse-nmap-file "mssql-udp.xml"))
(defparameter h (parse-nmap-file "localhost.xml"))

;; to make a new scan (nmap must be installed obviously).
(defparameter h (run-nmap "-T4 -F 192.168.1.0/24"))

Examples:
(nmap-keys h)
(nmap-val h :scaninfo)
(nmap-val h :scaninfo :protocol)
(nmap-hosts h)
(nmap-host h 0)
(nmap-host h 0 :address-addr-1)

(map-host-vals h :address-addr-1)
(map-hosts h :status-state "up")
(map-ports-vals h 0 :portid)

|#


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun get-ip (host)
  "Returns the ipv4."
  (get-value-in host :address :addr))

(defun open-mssql-udp-p (ports)
  "This test assumes that only a single UDP port was scanned."
  (rassoc "udp-response" (aref ports 0) :test #'string=))

(defun get-open-mssql-udp (nmap-hash)
  "Returns a list hosts who responded to a UDP scan with 'udp-response'."
  (remove nil
          (loop for i across (nmap-hosts nmap-hash)
             collect
               (let ((r (open-mssql-udp-p (cdr (assoc :ports i)))))
                 (when r i)))))

(defun format-open-mssql-udp (host)
  (let* ((ip (get-ip host))
         (port-type "udp")
         (port 1434)
         (state (get-value-in host :ports :state))
         (response (get-value-in host :ports :reason))
         (name (get-value-in host :ports :name))
         (product (get-value-in host :ports :product))
         (version (get-value-in host :ports :version))
         (server-name (get-value-in host :scripts :windows-server-name))
         (instance (get-value-in host :scripts :instance-name))
         (tcp-port (get-value-in host :scripts :tcp-port))
         (service-pack-level (get-value-in host :scripts :service-pack-level))
         (post-sp-patches-applied (get-value-in host :scripts :post-sp-patches-applied))
         (named-pipe (get-value-in host :scripts :named-pipe)))
    (list ip port-type port state response name product version server-name
          instance tcp-port service-pack-level post-sp-patches-applied named-pipe)))

(defun scan-mssql-udp (ips)
  (let* ((args (format nil "-T4 -sU -sV --open -PS1434 -p1434 --script ms-sql-info --script-args mssql.instance-port=1433 ~A" ips))
         (h (run-nmap args))
         (hosts (get-open-mssql-udp h)))
    (list (mapcar #'format-open-mssql-udp hosts)
          h)))
