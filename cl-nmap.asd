(in-package :cl-user)
(require :asdf)

(defpackage :cl-nmap-asd
  (:use :cl :asdf))

(in-package :cl-nmap-asd)

(defsystem :cl-nmap
  :description "A Common Lisp interface to nmap."
  :version "0.1"
  :author "Michael J. Rossi <dionysius.rossi@gmail.com>"
  :licence "Public Domain"
  :components ((:module "src"
                        :components
                        ((:file "cl-nmap"))))
  :depends-on (:alexandria :cxml :uiop)
  )
