# cl-nmap

A Common Lisp interface to Nmap.

This is the equilivent of python-nmap, but in Common Lisp...because it's fun.

# Overview

Assuming you have nmap installed, load this and run `scan`.

The returned value is a hash, some of which values of which are a vector of alists.

To explore, try `(hash-keys result-hash)` to explore.

# Usage

# Todo

Add proper package exports.
Add better Usage/overview/examples.
Add async scans.

# License

Copyright (c) 2018 Michael J. Rossi

Licensed under the MIT License.
